import { Observer, Observable, of } from 'rxjs';
import { LostItem } from './LostItem';
import { FoundItem } from './FoundItem';

export class Role {
  static USER = 'USER';
  static ADMIN = 'ADMIN';
}

export class User {
  public id: number;
  public fullName: string;
  username: string;
  password: string;
  public email: string;
  role: string;
  public lostItems: Observable<LostItem[]>;
  public foundItems: Observable<FoundItem[]>;

  constructor(
    username?: string,
    fullName?: string,
    password?: string,
    email?: string,
    role?: string
  ) {
    this.username = username;
    this.fullName = fullName;
    this.password = password;
    this.email = email;
    this.role = role;
    this.lostItems = of([
      new LostItem(
        12,
        'Lost bounty hunter',
        new File([], 'bountyhunter.jpg'),
        'Lost somewhere is space',
        [],
        'pls found him :(',
        this,
        new Date('2020-01-01'),
        [],
        []
      ),
      new LostItem(
        12,
        'Lost bounty hunter',
        new File([], 'bountyhunter.jpg'),
        'Lost somewhere is space',
        [],
        'pls found him :(',
        this,
        new Date('2020-01-01'),
        [],
        []
      ),
       new LostItem(
        12,
        'Lost bounty hunter',
        new File([], 'bountyhunter.jpg'),
        'Lost somewhere is space',
        [],
        'pls found him :(',
        this,
        new Date('2020-01-01'),
        [],
        []
      ),
      new LostItem(
        12,
        'Lost bounty hunter',
        new File([], 'bountyhunter.jpg'),
        'Lost somewhere is space',
        [],
        'pls found him :(',
        this,
        new Date('2020-01-01'),
        [],
        []
      )
    ]);
    this.foundItems = of([
      new FoundItem(
        12,
        'Found bounty hunter',
        new File([], 'bountyhunter.jpg'),
        [],
        'Lost somewhere is space',
        'pls found him :(',
        this,
        new Date('2020-01-01'),
        []
      ),
    ]);
  }
}
