import { FoundItem } from './FoundItem';


export class AnswerForLostItem {
  id: number;
  message: string;
  foundItem: FoundItem;
  constructor(id: number, message: string, foundItem: FoundItem) {
    this.id = id;
    this.message = message;
    this.foundItem = foundItem;
  }
}
