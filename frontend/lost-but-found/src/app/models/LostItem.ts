import { User } from './User';
import { Label } from './Label';
import { formatDate } from '@angular/common';
import { AnswerForLostItem } from './AnswerForLostItem';
import { Place } from './Place';

export class LostItem {

  id: number;
  name: string;
  picture: File;
  lostItHereDescription: string;
  itemDescription: string;
  searcher: User;
  dateOfLost: Date;
  labels: Array<Label>;
  answers: Array<AnswerForLostItem>;
  lostItHerePlace: Array<Place>;
  constructor(
    id?: number,
    name?: string,
    picture?: File,
    lostItHereDescription?: string,
    lostItHerePlace?: Array<Place>,
    itemDescription?: string,
    searcher?: User,
    dateOfLost?: Date,
    labels?: Array<Label>,
    answers?: Array<AnswerForLostItem>
    ) {
      this.id = id;
      this.dateOfLost = dateOfLost;
      this.itemDescription = itemDescription;
      this.lostItHereDescription = lostItHereDescription;
      this.name = name;
      this.picture = picture;
      this.searcher = searcher;
      this.labels = labels;
      this.answers = answers;
      this.lostItHerePlace = lostItHerePlace;

  }



  /**
   * Template description for item card
   */
  public getTempDescription() {
    if ( this.itemDescription.length > 200) {
      return this.itemDescription.substring(0, 200) + '...';
    } else {
      return this.itemDescription;
    }

  }

  /**
   * name
   */
  public dateOfLostDateFormat(): string {
    return formatDate(this.dateOfLost, 'yyyy-MM-dd', 'en_US');
  }


}
