import { User } from './User';
import { Label } from './Label';
import { formatDate } from '@angular/common';
import { Place } from './Place';


export class FoundItem {
id: number;
name: string;
picture: File;
foundItHereDescreption: string;
itemDescription: string;
founder: User;
dateOfFound: Date;
labels: Label[];
foundItHere: Place[];

constructor(
  id?: number,
  name?: string,
  picture?: File,
  foundItHerePlace?: Place[],
  foundItHereDescription?: string,
  itemDescription?: string,
  founder?: User,
  dateOfFound?: Date,
  labels?: Array<Label>
) {
  this.id = id;
  this.name = name;
  this.picture = picture;
  this.foundItHereDescreption = foundItHereDescription;
  this.itemDescription = itemDescription;
  this.founder = founder;
  this.dateOfFound = dateOfFound;
  this.labels = labels;
  this.foundItHere = foundItHerePlace;
}

  /**
   * Template description for item card
   */
  public getTempDescription() {
    if ( this.itemDescription.length > 200) {
      return this.itemDescription.substring(0, 200) + '...';
    } else {
      return this.itemDescription;
    }

  }

  /**
   * name
   */
  public dateOfLostDateFormat(): string {
    return formatDate(this.dateOfFound, 'yyyy-MM-dd', 'en_US');
  }



}
