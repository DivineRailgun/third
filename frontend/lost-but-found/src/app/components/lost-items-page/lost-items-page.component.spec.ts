import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LostItemsPageComponent } from './lost-items-page.component';
import { MatMenuModule } from '@angular/material/menu';

describe('LostItemsPageComponent', () => {
  let component: LostItemsPageComponent;
  let fixture: ComponentFixture<LostItemsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LostItemsPageComponent ],
      imports: [MatMenuModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LostItemsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
