import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnswerForLostItemsComponent } from './answer-for-lost-items.component';
import { MatMenuModule } from '@angular/material/menu';

describe('AnswerForLostItemsComponent', () => {
  let component: AnswerForLostItemsComponent;
  let fixture: ComponentFixture<AnswerForLostItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnswerForLostItemsComponent ],
      imports: [MatMenuModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnswerForLostItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


});
