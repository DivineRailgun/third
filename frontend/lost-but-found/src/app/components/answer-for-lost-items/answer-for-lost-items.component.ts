import { Component, OnInit, Input } from '@angular/core';
import { AnswerForLostItem } from 'src/app/models/AnswerForLostItem';
import { FoundItem } from 'src/app/models/FoundItem';

@Component({
  selector: 'app-answer-for-lost-items',
  templateUrl: './answer-for-lost-items.component.html',
  styleUrls: ['./answer-for-lost-items.component.css']
})
export class AnswerForLostItemsComponent implements OnInit {

  @Input() answer: AnswerForLostItem;
  constructor() {
   }


  ngOnInit(): void {
  }

}
