import { ActivatedRoute, RouterModule } from '@angular/router';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LostItemDetailComponent } from './lost-item-detail.component';
import { MatMenuModule } from '@angular/material/menu';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClientModule } from '@angular/common/http';

describe('LostItemDetailComponent', () => {
  let component: LostItemDetailComponent;
  let fixture: ComponentFixture<LostItemDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LostItemDetailComponent ],
      imports: [ MatMenuModule, HttpClientTestingModule, HttpClientModule, RouterModule.forRoot([]), ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LostItemDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


});
