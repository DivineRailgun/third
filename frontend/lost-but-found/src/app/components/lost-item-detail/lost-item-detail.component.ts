import { browser } from 'protractor';

import { Component, OnInit } from '@angular/core';
import { LostItem } from 'src/app/models/LostItem';
import { LostItemService } from 'src/app/services/lost-item.service';
import { ActivatedRoute, Router } from '@angular/router';
import { formatDate } from '@angular/common';
import { Label } from 'src/app/models/Label';

@Component({
  selector: 'app-lost-item-detail',
  templateUrl: './lost-item-detail.component.html',
  styleUrls: ['./lost-item-detail.component.css']
})
export class LostItemDetailComponent implements OnInit {


  lostItem: LostItem = null;
  id: number;

  constructor(private lostItemService: LostItemService,
              private route: ActivatedRoute,
              private router: Router) {
        this.route.params.subscribe(
          params => this.id = params.id,

        );

   }

  async ngOnInit(): Promise<void> {
    this.lostItem = await this.lostItemService.getLostItem(this.id);
    //  temporary for UI
    this.lostItem.dateOfLost = new Date('2020-04-04');
    this.lostItem.picture = new File([], 'pig.png');
    this.lostItem.itemDescription = 'asdasdasdasd';
    this.lostItem.labels = [(new Label(0, 'Wooden'))];

  }

  getDateOfLostInFormat() {
    return formatDate(this.lostItem.dateOfLost, 'yyyy-MM-dd', 'en_US');
  }



}
