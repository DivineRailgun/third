import {  MatMenuModule } from '@angular/material/menu';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LostItemListComponent } from './lost-item-list.component';
import { of } from 'rxjs/internal/observable/of';
import { NgForOf } from '@angular/common';
import { LostItem } from 'src/app/models/LostItem';
import { User } from 'src/app/models/User';
import { Label } from 'src/app/models/Label';
import { DebugElement } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClientModule } from '@angular/common/http';

describe('LostItemListComponent', () => {
  let component: LostItemListComponent;
  let fixture: ComponentFixture<LostItemListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LostItemListComponent ],
      imports: [ MatMenuModule, HttpClientModule, HttpClientTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LostItemListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();


  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should give back only filtered items by name', () => {

    const searchText = 'Pig';
    component.searchOptions.searchText = searchText; // should work with uppe/lower case too
    component.refreshItems();
    component.lostItems.subscribe(array => {
      array.forEach(item => {
        const text = searchText.toLowerCase();
        expect(
          item.name.toLowerCase().includes(text) ||
          item.itemDescription.toLowerCase().includes(text) ||
          item.lostItHereDescription.toLowerCase().includes(text) ||
          item.searcher.username.toLowerCase().includes(text)
          ).toBeTruthy();
      });
    });
  });


  it('should give back only filtered items by label', () => {
    const labels = [{ id: 1, name: 'Wooden' }];
    component.searchOptions.labels = labels;
    const labelNames = component.searchOptions.labels.map(label => label.name);
    component.refreshItems();
    component.lostItems.subscribe(array => {
      array.forEach(item => {

        expect(item.labels.filter(label => labelNames.includes(label.name)).length).toBe(labels.length);
      });
    });
  });


});
