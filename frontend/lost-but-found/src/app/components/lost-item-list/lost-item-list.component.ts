
import { CommonModule } from '@angular/common';
import { Component, OnInit, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { FormBuilder, FormGroup } from '@angular/forms';
import { DataSource } from '@angular/cdk/collections';
import { LostItem } from 'src/app/models/LostItem';
import { LostItemService } from 'src/app/services/lost-item.service';
import { Observable } from 'rxjs';



@Component({
  selector: 'app-lost-item-list',
  templateUrl: './lost-item-list.component.html',
  styleUrls: ['./lost-item-list.component.css']
})
export class LostItemListComponent implements OnInit {

  filters = [];
  selectedFilters = [];
  dropdownSettings = {};

  latestFirst = true;
  lostItems: Observable< LostItem[]>;
  searchText = '';
  searchOptions = {
    searchText : '',
    labels : [],
    latest: true
  };


  constructor(private lostItemService: LostItemService) {

  }

  ngOnInit(): void {
    this.lostItems = this.lostItemService.getLostItems(this.searchOptions);

    this.filters = [
      { id: 1, name: 'Clothes' },
      { id: 2, name: 'Electronical' },
      { id: 3, name: 'Wallet' },
      { id: 4, name: 'Pet' },
      { id: 5, name: 'Wooden' },
    ];

    this.selectedFilters = [

    ];

    this.dropdownSettings  = {
      singleSelection: false,
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      idField: 'id',
      textField: 'name',
      itemsShowLimit: 2,
      allowSearchFilter: true
    };


  }

  onItemSelect(item: any) {
    this.refreshItems();
  }
  onSelectAll(items: any) {
  }

  setLatestFirst(latest: boolean) {
    this.searchOptions.latest = latest;
    const selectorForThis = '#' + (latest ? 'latest' : 'oldest');
    const selectorForOther = '#' + (latest ? 'oldest' : 'latest');
    document.querySelector(selectorForThis).classList.add('choosen-sort');
    document.querySelector(selectorForOther).classList.remove('choosen-sort');


    this.refreshItems();


  }

  refreshItems() {
    this.lostItems = this.lostItemService.getLostItems(this.searchOptions);
  }

}
/*
export class lostItemDataSource extends DataSource<any> {
  constructor(private lostItemService: LostItemService) {
    super();
  }

  connect(): LostItem[] {
    return this.lostItemService.
  }

  disconnect() {
  }
}
*/
