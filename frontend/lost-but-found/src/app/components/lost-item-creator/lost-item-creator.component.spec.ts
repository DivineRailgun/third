import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LostItemCreatorComponent } from './lost-item-creator.component';

describe('LostItemCreatorComponent', () => {
  let component: LostItemCreatorComponent;
  let fixture: ComponentFixture<LostItemCreatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LostItemCreatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LostItemCreatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


});
