import { LabelService } from './../../services/label.service';
import { LostItem } from './../../models/LostItem';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from 'src/app/models/User';
import { LostItemService } from 'src/app/services/lost-item.service';
import { map } from 'rxjs/operators';
import { Label } from 'src/app/models/Label';
import { PlaceService } from 'src/app/services/place.service';

@Component({
  selector: 'app-lost-item-creator',
  templateUrl: './lost-item-creator.component.html',
  styleUrls: ['./lost-item-creator.component.css'],
})
export class LostItemCreatorComponent implements OnInit {
  items: any;
  lostItemForm: any;
  pictureImgUrl: any;
  filters: Label[] ;
  selectedFilters = [];
  dropdownSettings = {};
  labels = [];
  file: any;
  fileInBinary: any;
  hideFileChooser = false;
  validationMessages: any;
  validateOn = false;
  lostItHerePlaces = [];
  lostItem = {
    id: 0,
    name: '',
    picture: {
      name: '',
      url: ''
    },
    lostItHereDescription: '',
    lostItHerePlace : {
      id: 0,
      name: ''
    },
    labels: [],
    itemDescription: '',
    searcher: null,
    dateOfLost: null,
    answers: [],


  };


  constructor(
    public dialogRef: MatDialogRef<LostItemCreatorComponent>,
    private sanitizer: DomSanitizer,
    private formBuilder: FormBuilder,
    private lostItemService: LostItemService,
    private labelService: LabelService,
    private placeService: PlaceService
  ) {
    this.lostItemForm = this.formBuilder.group({
      name: new FormControl('', Validators.required),
      itemDescription: new FormControl('', Validators.required),
      picture: null,
      dateOfLost: new FormControl('', Validators.required),
      lostItHereDescription: new FormControl('', Validators.required),
      lostItHerePlace: new FormControl('', Validators.required ),
      labels: '',
    });

    this.selectedFilters = [];

    this.dropdownSettings = {
      singleSelection: false,
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      idField: 'id',
      textField: 'name',
      itemsShowLimit: 2,
      allowSearchFilter: true,
    };
    this.file = null;
    this.validationMessages = {
      name: [
        { type: 'required', message: 'Item name is required' },
      ],
      itemDescription: [
        { type: 'required', message: 'Item description is required' },
      ],
      dateOfLost: [
        { type: 'required', message: 'The date of loss is required' },
      ],
      lostItHereDescription: [
        { type: 'required', message: 'Description of loss is required' },
      ],
      lostItHerePlace : [
        { type: 'required', message: 'Place of loss is required' },
      ],
    };

  }

  ngOnInit(): void {
      this.labelService.getLabels().subscribe(data => this.filters = data);
      this.placeService.getPlaces().subscribe(data =>  this.lostItHerePlaces = data);
  }

  closeModal() {
    this.dialogRef.close();
  }

  onSubmit(value: any) {
    if (this.lostItemForm.valid) {
      const emittedItem = Object.assign(this.lostItem, value);
      emittedItem.picture = this.file;
      this.lostItemService.createLostItem(emittedItem);
      console.log(emittedItem);
      this.closeModal();


    } else {
      this.validateOn = true;
    }


  }

  uploadFile(event: any) {
    const e = event[0];
    this.file = e;

    const reader = new FileReader();
    reader.onload = () => {
      this.file =  reader.result.toString();
      console.log(this.file);
    };
    reader.readAsDataURL(this.file);



    this.pictureImgUrl = this.sanitizer.bypassSecurityTrustUrl(
       window.URL.createObjectURL(this.file)
    );

    this.hideFileChooser = true;

  }
  deleteAttachment() {
    this.file = null;
    this.hideFileChooser = false;
  }
}
