import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoundItemDetailComponent } from './found-item-detail.component';

describe('FoundItemDetailComponent', () => {
  let component: FoundItemDetailComponent;
  let fixture: ComponentFixture<FoundItemDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoundItemDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoundItemDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


});
