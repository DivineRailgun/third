import { Component, OnInit } from '@angular/core';
import { FoundItem } from 'src/app/models/FoundItem';
import { FoundItemService } from 'src/app/services/found-item.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Label } from 'src/app/models/Label';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-found-item-detail',
  templateUrl: './found-item-detail.component.html',
  styleUrls: ['./found-item-detail.component.css']
})
export class FoundItemDetailComponent implements OnInit {

  foundItem: FoundItem;
  id: number;
  constructor(
    private foundItemService: FoundItemService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.route.params.subscribe((params) => (this.id = params.id));
  }


  async ngOnInit(): Promise<void> {
    this.foundItem = await this.foundItemService.getFoundItem(this.id);
    //  temporary for UI
    this.foundItem.dateOfFound = new Date('2020-04-04');
    this.foundItem.picture = new File([], 'pig.png');
    this.foundItem.itemDescription = 'asdasdasdasd';
    this.foundItem.labels = [(new Label(0, 'Wooden'))];

  }

  getDateOfLostInFormat() {
    return formatDate(this.foundItem.dateOfFound, 'yyyy-MM-dd', 'en_US');
  }

}
