import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoundItemCreatorComponent } from './found-item-creator.component';

describe('FoundItemCreatorComponent', () => {
  let component: FoundItemCreatorComponent;
  let fixture: ComponentFixture<FoundItemCreatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoundItemCreatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoundItemCreatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


});
