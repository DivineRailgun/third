import { Component, OnInit } from '@angular/core';
import { FoundItem } from 'src/app/models/FoundItem';
import { User } from 'src/app/models/User';
import { MatDialogRef } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { FoundItemService } from 'src/app/services/found-item.service';
import { LabelService } from 'src/app/services/label.service';
import { PlaceService } from 'src/app/services/place.service';

@Component({
  selector: 'app-found-item-creator',
  templateUrl: './found-item-creator.component.html',
  styleUrls: ['./found-item-creator.component.css']
})
export class FoundItemCreatorComponent implements OnInit {
  items: any;
  foundItemForm: any;
  pictureImgUrl: any;
  filters = [];
  selectedFilters = [];
  dropdownSettings = {};
  labels = [];
  file: any;
  hideFileChooser = false;
  validationMessages: any;
  validateOn = false;
  foundItHerePlaces = [];
  foundItem = {
    id: 0,
    name: '',
    picture: {
      name: '',
      url: ''
    },
    foundItHereDescription: '',
    foundItHerePlace : {
      id: 0,
      name: ''
    },
    labels: [],
    itemDescription: '',
    searcher: null,
    dateOfFound: null,
    answers: [],


  };

  constructor(
    public dialogRef: MatDialogRef<FoundItemCreatorComponent>,
    private sanitizer: DomSanitizer,
    private formBuilder: FormBuilder,
    private foundItemService: FoundItemService,
    private labelService: LabelService,
    private placeService: PlaceService
  ) {
    this.foundItemForm = this.formBuilder.group({
      name: new FormControl('', Validators.required),
      itemDescription: new FormControl('', Validators.required),
      picture: null,
      dateOfFound: new FormControl('', Validators.required),
      foundItHerePlace: new FormControl('', Validators.required ),
      foundItHereDescription: new FormControl('', Validators.required),
      labels: '',
    });



    this.dropdownSettings = {
      singleSelection: false,
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      idField: 'id',
      textField: 'name',
      itemsShowLimit: 2,
      allowSearchFilter: true,
    };
    this.file = null;
    this.validationMessages = {
      name: [
        { type: 'required', message: 'Item name is required' },
      ],
      itemDescription: [
        { type: 'required', message: 'Item description is required' },
      ],
      dateOfFound: [
        { type: 'required', message: 'The date of found is required' },
      ],
      foundItHereDescription: [
        { type: 'required', message: 'Description of found is required' },
      ],
      foundtItHerePlace : [
        { type: 'required', message: 'Place of found is required' },
      ],
    };

  }

  ngOnInit(): void {
      this.labelService.getLabels().subscribe(data => this.filters = data);
      this.placeService.getPlaces().subscribe(data =>  this.foundItHerePlaces = data);
  }

  closeModal() {
    this.dialogRef.close();
  }

  onSubmit(value: any) {
    if (this.foundItemForm.valid) {
      const emittedItem = Object.assign(this.foundItem, value);
      emittedItem.picture = this.file;
      this.foundItemService.createFoundItem(emittedItem);
      console.log(emittedItem);
      this.closeModal();
    } else {
      this.validateOn = true;
    }


  }

  uploadFile(event: any) {
    const e = event[0];
    this.file = e;

    const reader = new FileReader();
    reader.onload = () => {
      this.file =  reader.result.toString();
      console.log(this.file);
    };
    reader.readAsDataURL(this.file);



    this.pictureImgUrl = this.sanitizer.bypassSecurityTrustUrl(
       window.URL.createObjectURL(this.file)
    );

    this.hideFileChooser = true;
  }
  deleteAttachment() {
    this.file = null;
    this.hideFileChooser = false;
  }


}
