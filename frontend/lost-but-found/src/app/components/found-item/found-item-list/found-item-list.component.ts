import { FoundItemService } from 'src/app/services/found-item.service';
import { Component, OnInit } from '@angular/core';
import { FoundItem } from 'src/app/models/FoundItem';
import { Observable } from 'rxjs';
import { LostItemService } from 'src/app/services/lost-item.service';

@Component({
  selector: 'app-found-item-list',
  templateUrl: './found-item-list.component.html',
  styleUrls: ['./found-item-list.component.css']
})
export class FoundItemListComponent implements OnInit {

  filters = [];
  selectedFilters = [];
  dropdownSettings = {};

  latestFirst = true;
  foundItems: Observable< FoundItem[]>;
  searchText = '';
  searchOptions = {
    searchText : '',
    labels : [],
    latest: true
  };


  constructor(private foundItemService: FoundItemService) { }

  ngOnInit(): void {
    this.foundItems = this.foundItemService.getFoundItems(this.searchOptions);

    this.filters = [
      { id: 1, name: 'Clothes' },
      { id: 2, name: 'Electronical' },
      { id: 3, name: 'Wallet' },
      { id: 4, name: 'Pet' },
      { id: 5, name: 'Wooden' },
    ];

    this.selectedFilters = [

    ];

    this.dropdownSettings  = {
      singleSelection: false,
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      idField: 'id',
      textField: 'name',
      itemsShowLimit: 2,
      allowSearchFilter: true
    };

  }
  onItemSelect(item: any) {
    this.refreshItems();
  }
  onSelectAll(items: any) {
  }

  setLatestFirst(latest: boolean) {
    this.searchOptions.latest = latest;
    const selectorForThis = '#' + (latest ? 'latest' : 'oldest');
    const selectorForOther = '#' + (latest ? 'oldest' : 'latest');
    document.querySelector(selectorForThis).classList.add('choosen-sort');
    document.querySelector(selectorForOther).classList.remove('choosen-sort');


    this.refreshItems();


  }

  refreshItems() {
    this.foundItems = this.foundItemService.getFoundItems(this.searchOptions);
  }


}
