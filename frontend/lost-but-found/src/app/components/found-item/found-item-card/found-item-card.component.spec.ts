import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoundItemCardComponent } from './found-item-card.component';

describe('FoundItemCardComponent', () => {
  let component: FoundItemCardComponent;
  let fixture: ComponentFixture<FoundItemCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoundItemCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoundItemCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


});
