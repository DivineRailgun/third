import { Component, OnInit, Input } from '@angular/core';
import { FoundItem } from 'src/app/models/FoundItem';

@Component({
  selector: 'app-found-item-card',
  templateUrl: './found-item-card.component.html',
  styleUrls: ['./found-item-card.component.css']
})
export class FoundItemCardComponent implements OnInit {

  @Input() foundItem: FoundItem;

  constructor() { }

  ngOnInit(): void {
  }

}
