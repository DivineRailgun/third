import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoundItemsPageComponent } from './found-items-page.component';

describe('FoundItemsPageComponent', () => {
  let component: FoundItemsPageComponent;
  let fixture: ComponentFixture<FoundItemsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoundItemsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoundItemsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


});
