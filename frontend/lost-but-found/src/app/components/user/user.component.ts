import { AuthService } from 'src/app/services/auth.service';
import { LostItemCreatorComponent } from './../lost-item-creator/lost-item-creator.component';
import { Component, OnInit, ViewEncapsulation, Inject, Renderer2, OnDestroy } from '@angular/core';
import { User, Role } from 'src/app/models/User';
import { PageScrollService } from 'ngx-page-scroll-core';
import { DOCUMENT } from '@angular/common';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { FoundItemCreatorComponent } from '../found-item/found-item-creator/found-item-creator.component';




@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class UserComponent implements OnInit, OnDestroy {

  user: User;
  constructor(
    private pageScrollService: PageScrollService,
    @Inject(DOCUMENT) private document,
    public matDialog: MatDialog,
    private authService: AuthService
  ) {
    // new User('babyoda1', 'Baby Yoda', 'passwoRd', 'baby@yoda.sw', Role.USER);
  }

  ngOnInit(): void {
    this.user = this.authService.user;
    this.document.body.classList.add('bground');

  }

  ngOnDestroy() {
    this.document.body.classList.remove('bground');
  }

  openNewLostItemModal() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.id = 'modal-component';
    dialogConfig.width = '700px';
    const modalDialog = this.matDialog.open(LostItemCreatorComponent, dialogConfig);
  }

  openNewFoundItemModal() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.id = 'modal-component';
    dialogConfig.width = '700px';
    const modalDialog = this.matDialog.open(FoundItemCreatorComponent, dialogConfig);
  }

}
