import { Component, OnInit, Input } from '@angular/core';
import { LostItem } from 'src/app/models/LostItem';
import { User } from 'src/app/models/User';

@Component({
  selector: 'app-lost-item-card',
  templateUrl: './lost-item-card.component.html',
  styleUrls: ['./lost-item-card.component.css']

})
export class LostItemCardComponent implements OnInit {

  @Input() lostItem: LostItem;

  constructor() {
    /*this.lostItem;  = new LostItem(
      0,
      'Lost tissue',
      new File([], 'test.png'),
      'I have lost it :(',
      `Elvesztettem zsebkendőmet,
      Szidott anyám érte.
      Annak, aki visszaadja,
      Csókot adok érte.

      Szabad péntek, szabad szombat,
      Szabad szappanozni.
      Szabad az én galambomnak
      Egy pár csókot adni.`,
      new User('Jani', 'test', 'asd@asd.asd', 'USER'),
      new Date('2020-04-04'),
    );*/

  }

  ngOnInit(): void {

  }



}
