import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LostItemCardComponent } from './lost-item-card.component';
import { MatMenuModule } from '@angular/material/menu';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('LostItemCardComponent', () => {
  let component: LostItemCardComponent;
  let fixture: ComponentFixture<LostItemCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LostItemCardComponent ],
      imports: [ MatMenuModule, HttpClientTestingModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LostItemCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

});
