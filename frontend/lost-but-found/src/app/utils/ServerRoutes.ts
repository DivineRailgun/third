export class Routes {
  static LOGIN = 'user/login';
  static LOGOUT = 'user/logout';
  static LOST_ITEMS = 'lost_items';
  static FOUND_ITEMS = 'found_items';
}

export class Server {
  private static address = 'localhost';
  private static port = '8080';


  static routeTo(route: string) {
    return `http://${Server.address}:${Server.port}/${route}`;
  }
}
