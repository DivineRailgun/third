import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Label } from '../models/Label';
import { map } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
  })
};

@Injectable({
  providedIn: 'root'
})
export class LabelService {

  constructor(private httpClient: HttpClient) { }

  getLabels() {
    // returns all the labels
    return of([
      new Label(0, 'Clothes'),
      new Label(1, 'Electronical'),
      new Label(2, 'Wallet'),
      new Label(3, 'Pet'),
      new Label(4, 'Wooden')
    ]);
  }
}
