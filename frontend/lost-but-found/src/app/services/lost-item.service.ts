import { HttpClient, HttpHeaders } from '@angular/common/http';
import { browser } from 'protractor';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { of } from 'rxjs/internal/observable/of';
import { map, filter, first, tap } from 'rxjs/operators';
import { LostItem } from '../models/LostItem';
import { User } from '../models/User';
import { Label } from '../models/Label';
import { AnswerForLostItem } from '../models/AnswerForLostItem';
import { FoundItem } from '../models/FoundItem';
import { Server, Routes } from '../utils/ServerRoutes';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json; charset=UTF-8'
  })
};

@Injectable({
  providedIn: 'root'
})
export class LostItemService {
  tempLostItems: Observable<LostItem[]>;


  constructor( private httpClient: HttpClient) {
    this.tempLostItems = of([
      new LostItem(
        0,
        'Lost tissue',
        new File([], 'tissue.jpg'),
        'I have lost it :(',
        [],
        `Elvesztettem zsebkendőmet,
        Szidott anyám érte.
        Annak, aki visszaadja,
        Csókot adok érte.

        Szabad péntek, szabad szombat,
        Szabad szappanozni.
        Szabad az én galambomnak
        Egy pár csókot adni.`,
        new User('Jani', 'test', 'asd@asd.asd', 'USER'),
        new Date('2020-04-04'),
        [
          new Label(0, 'Wooden')
        ]
      ),
      new LostItem(
        1,
        'Lost pig',
        new File([], 'pig.png'),
        'It was cute',
        [],
        `GIVE ME BACK MY LITTLE PIG!!`,
        new User('PigOwner33', 'pigtest', 'pig@asd.asd', 'USER'),
        new Date('2020-03-22'),
        []
      )
      ]);

  }


  getLostItems(searchOption: any): Observable<LostItem[]> {
    const text = searchOption.searchText.toLowerCase();

    const labels = searchOption.labels.map(label => label.name);
    const latest = searchOption.latest;
    return this.tempLostItems.pipe(
      map(
        lostItems => lostItems.filter(
          item => (
            (item.name.toLowerCase().includes(text) ||
            item.itemDescription.toLowerCase().includes(text) ||
            item.lostItHereDescription.toLowerCase().includes(text) ||
            item.searcher.username.toLowerCase().includes(text)) &&
            (labels.length === 0 ||
            item.labels.filter(label => labels.includes(label.name)).length === labels.length)
          )
        ).sort((a, b) => latest ? a.dateOfLost.getDate() - b.dateOfLost.getDate() : b.dateOfLost.getDate() - a.dateOfLost.getDate() )
      )
    );
  }

  getTestLostItem(): LostItem  {
    return new LostItem(
      0,
      'Lost tissue',
      new File([], 'tissue.jpg'),
      'I have lost it :(',
      [],
      `Elvesztettem zsebkendőmet,
      Szidott anyám érte.
      Annak, aki visszaadja,
      Csókot adok érte.

      Szabad péntek, szabad szombat,
      Szabad szappanozni.
      Szabad az én galambomnak
      Egy pár csókot adni.`,
      new User('Jani', 'test', 'asd@asd.asd', 'USER'),
      new Date('2020-04-04'),
      [
        new Label(1, 'Tissue'),
        new Label(2, 'Wooden'),
      ],
      [
        new AnswerForLostItem(
          0,
          'I found it!',
          new FoundItem(
            0,
            'Found tissue',
            new File([], ''),
            [],
            'It was in the toilet',
            'Good one',
            new User(
              'Gezu', '', '', 'USER'
            ),

            new Date('2020-04-05'),
            []
          )
        ),
        new AnswerForLostItem(
          0,
          'I have it!',
          new FoundItem(
            0,
            'Found tissue',
            new File([], ''),
            [],
            'It was in the somewhere',
            'Good one',
            new User(
              'Ferike', '', '', 'USER'
            ),

            new Date('2020-04-05'),
            []
          )
        )
      ]
    );
  }

  getLostItem(id: number): Promise<LostItem> {
      return this.httpClient.get<LostItem>(Server.routeTo(Routes.LOST_ITEMS) + '/' + id, httpOptions).toPromise();
  }

  deleteLostItem(id: number): Promise<LostItem> {
    return this.httpClient.delete<LostItem>(Server.routeTo(Routes.LOST_ITEMS) + '/' + id, httpOptions).toPromise();
  }

  createLostItem(item: LostItem): Promise<LostItem> {
    return this.httpClient.post<LostItem>(Server.routeTo(Routes.LOST_ITEMS) , item, httpOptions).toPromise();
  }




/*
create(lostItem: LostItem): LostItem {
  this.tempLostItems.(lostItem);
  return lostItem;
}

delete(id: number) {
  this.tempLostItems.splice(id);
}

read(id: number) {
  return this.tempLostItems[id];
}

update(lostItem: LostItem) {
  //  TODO
}

*/
}
