import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { FoundItem } from '../models/FoundItem';
import { Server, Routes } from '../utils/ServerRoutes';
import { map } from 'rxjs/operators';
import { User } from '../models/User';
import { Label } from '../models/Label';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
  })
};

@Injectable({
  providedIn: 'root'
})
export class FoundItemService {

  tempFoundItems: Observable<FoundItem[]>;

  constructor(private httpClient: HttpClient) {
    this.tempFoundItems = of([
      new FoundItem(
        0,
        'Found tissue',
        new File([], 'tissue.jpg'),
        [],
        'I have lost it :(',
        `Elvesztettem zsebkendőmet,
        Szidott anyám érte.
        Annak, aki visszaadja,
        Csókot adok érte.

        Szabad péntek, szabad szombat,
        Szabad szappanozni.
        Szabad az én galambomnak
        Egy pár csókot adni.`,
        new User('Jani', 'test', 'asd@asd.asd', 'USER'),
        new Date('2020-04-04'),
        [
          new Label(0, 'Wooden')
        ]
      ),
      new FoundItem(
        1,
        'Found pig',
        new File([], 'pig.png'),
        [],
        'It was cute',
        `GIVE ME BACK MY LITTLE PIG!!`,
        new User('PigOwner33', 'pigtest', 'pig@asd.asd', 'USER'),
        new Date('2020-03-22'),
        []
      )
      ]);
  }

  getFoundItems(searchOption: any): Observable<FoundItem[]> {
    const text = searchOption.searchText.toLowerCase();

    const labels = searchOption.labels.map(label => label.name);
    const latest = searchOption.latest;
    return this.tempFoundItems.pipe(
      map(
        lostItems => lostItems.filter(
          item => (
            (item.name.toLowerCase().includes(text) ||
            item.itemDescription.toLowerCase().includes(text) ||
            item.foundItHereDescreption.toLowerCase().includes(text) ||
            item.founder.username.toLowerCase().includes(text)) &&
            (labels.length === 0 ||
            item.labels.filter(label => labels.includes(label.name)).length === labels.length)
          )
        ).sort((a, b) => latest ? a.dateOfFound.getDate() - b.dateOfFound.getDate() : b.dateOfFound.getDate() - a.dateOfFound.getDate() )
      )
    );
  }

  getFoundItem(id: number): Promise<FoundItem> {
    return this.httpClient.get<FoundItem>(Server.routeTo(Routes.FOUND_ITEMS) + '/' + id, httpOptions).toPromise();
  }

  deleteFoundItem(id: number): Promise<FoundItem> {
    return this.httpClient.delete<FoundItem>(Server.routeTo(Routes.FOUND_ITEMS) + '/' + id, httpOptions).toPromise();
  }

  createFoundItem(item: FoundItem): Promise<FoundItem> {
    return this.httpClient.post<FoundItem>(Server.routeTo(Routes.FOUND_ITEMS), httpOptions).toPromise();
  }


}
