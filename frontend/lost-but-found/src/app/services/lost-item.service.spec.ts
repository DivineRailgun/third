import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { LostItemService } from './lost-item.service';
import { HttpClientModule } from '@angular/common/http';




describe('LostItemService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
    }).compileComponents();
  });

  it('should be created', () => {
    const service: LostItemService = TestBed.inject(LostItemService);
    expect(service).toBeTruthy();
  });


  it('should give back only filtered items by label', () => {
    const service: LostItemService = TestBed.inject(LostItemService);
    const givenLabels = [{ id: 1, name: 'Wooden' }];
    const searchOptions = {
      searchText : '',
      labels : givenLabels,
      latest : true
    };
    const labelNames = searchOptions.labels.map(label => label.name);
    const filteredItems = service.getLostItems(searchOptions);
    filteredItems.subscribe(array => {
      array.forEach(item => {

        expect(item.labels.filter(label => labelNames.includes(label.name)).length).toBe(givenLabels.length);
      });
    });

  });

  it('should give back only filtered items by string', () => {
    const service: LostItemService = TestBed.inject(LostItemService);
    const searchedString = 'Tissue';
    const searchOptions = {
      searchText : searchedString,
      labels : [],
      latest : true
    };
    const filteredItems = service.getLostItems(searchOptions);
    filteredItems.forEach(array => {
      array.forEach(item => {
        expect(item.name).toContain(searchedString.toLowerCase());

      });
    });
  });

  it('should give back only filtered items ordered latest', () => {
    const service: LostItemService = TestBed.inject(LostItemService);
    const searchOptions = {
      searchText : '',
      labels : [],
      latest : true
    };
    const filteredItems = service.getLostItems(searchOptions);
    let previous;
    let ordered = true;
    filteredItems.forEach(array => array.forEach(item => {
      if (previous !== undefined) {
        if (item.dateOfLost.getDate() < previous) {
          ordered = false;
        }
      }
      previous = item.dateOfLost.getDate();
    }));
    expect(ordered).toBeTruthy();
  });

  it('should give back only filtered items ordered oldest', () => {
    const service: LostItemService = TestBed.inject(LostItemService);
    const searchOptions = {
      searchText : '',
      labels : [],
      latest : false
    };
    const filteredItems = service.getLostItems(searchOptions);
    let previous: any;
    let ordered = true;
    filteredItems.forEach(array => array.forEach(item => {
      if (previous !== undefined) {
        if (item.dateOfLost.getDate() > previous) {
          ordered = false;
        }
      }
      previous = item.dateOfLost.getDate();
    }));
    expect(ordered).toBeTruthy();
  });



});
