import { Injectable } from '@angular/core';
import { User, Role } from '../models/User';
import { HttpHeaders, HttpClient } from '@angular/common/http';


export const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: ''
  })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLoggedIn: boolean;
  user: User;
  redirectUrl = 'home';
  authString = 'Authorization';

  constructor(private http: HttpClient) {
  }

  async login(username: string, password: string): Promise<User> {
    try {
      const token = btoa(`${username}:${password}`);
      httpOptions.headers = httpOptions.headers.set('Authorization', `Basic ${token}`);
      // const user = await this.http.post<User>(`users/login`, {}, httpOptions).toPromise();
      // test
      const user = new User('babyoda1', 'Baby Yoda', 'passwoRd', 'baby@yoda.sw', Role.USER);
      this.isLoggedIn = true;
      this.user = user;
      return Promise.resolve(this.user);
    } catch (e) {
      console.log(e);
      return Promise.reject();
    }
  }

  logout() {
    httpOptions.headers = httpOptions.headers.set('Authorization', ``);
    this.isLoggedIn = false;
    this.user = null;

  }


  private httpOptions() {
    const headers = { 'Content-Type': 'application/json' };
    if (window.localStorage.getItem('token')) {
      headers[this.authString] = window.localStorage.getItem('token');
    }
    return {
      headers: new HttpHeaders(headers)
    };
  }
}
