import { UserComponent } from './../components/user/user.component';
import { FoundItemDetailComponent } from './../components/found-item/found-item-detail/found-item-detail.component';
import { FoundItemsPageComponent } from './../components/found-item/found-items-page/found-items-page.component';
import { LostItemDetailComponent } from './../components/lost-item-detail/lost-item-detail.component';
import { LostItemsPageComponent } from './../components/lost-items-page/lost-items-page.component';


import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from '../components/login/login.component';
import { HomeComponent } from '../components/home/home.component';
import { NotFoundComponent } from '../components/not-found/not-found.component';
import { LostItemCreatorComponent } from '../components/lost-item-creator/lost-item-creator.component';
import { AuthGuard } from '../auth.guard';



const routes: Routes = [

    { path: 'home', component: HomeComponent, canActivate: [AuthGuard]},
    { path: '', component: HomeComponent, canActivate: [AuthGuard]},
    { path: 'login', component: LoginComponent},
    { path: 'lost_items', component: LostItemsPageComponent, canActivate: [AuthGuard]},
    { path: 'found_items', component: FoundItemsPageComponent, canActivate: [AuthGuard]},
    { path: 'found_items/:id', component: FoundItemDetailComponent, canActivate: [AuthGuard]},
    { path: 'lost_items/:id', component: LostItemDetailComponent, canActivate: [AuthGuard]},
    { path: 'lost_items/add/new', component: LostItemCreatorComponent, canActivate: [AuthGuard]},
    { path: 'user', component: UserComponent, canActivate: [AuthGuard]},
    { path: '404', component: NotFoundComponent, canActivate: [AuthGuard]},
    { path: '**', redirectTo: '/404'}



];



@NgModule({
  imports: [ RouterModule.forRoot(routes)  ],
  exports: [ RouterModule ],
  declarations: []
})
export class RoutingModule { }
