import { DragDropDirective } from './directives/drag-drop-directives';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { HttpClientModule } from '@angular/common/http';

import { MatButtonModule } from '@angular/material/button';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgModule, Component } from '@angular/core';

import { RoutingModule } from './routing/routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { LostItemCardComponent } from './components/lost-item-card/lost-item-card.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { LostItemListComponent } from './components/lost-item-list/lost-item-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LostItemsPageComponent } from './components/lost-items-page/lost-items-page.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { HttpModule } from '@angular/http';

import { LostItemDetailComponent } from './components/lost-item-detail/lost-item-detail.component';
import { AnswerForLostItemsComponent } from './components/answer-for-lost-items/answer-for-lost-items.component';
import { FoundItemCardComponent } from './components/found-item/found-item-card/found-item-card.component';
import { FoundItemDetailComponent } from './components/found-item/found-item-detail/found-item-detail.component';
import { FoundItemListComponent } from './components/found-item/found-item-list/found-item-list.component';
import { FoundItemsPageComponent } from './components/found-item/found-items-page/found-items-page.component';
import { UserComponent } from './components/user/user.component';
import { NgxPageScrollCoreModule } from 'ngx-page-scroll-core';
import { NgxPageScrollModule } from 'ngx-page-scroll';
import { LostItemCreatorComponent } from './components/lost-item-creator/lost-item-creator.component';
import { MatFormField, MatFormFieldModule } from '@angular/material/form-field';
import { MatNativeDateModule } from '@angular/material/core';
import { FoundItemCreatorComponent } from './components/found-item/found-item-creator/found-item-creator.component';





@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    LostItemCardComponent,
    NotFoundComponent,
    LostItemListComponent,
    LostItemsPageComponent,
    ToolbarComponent,
    LostItemDetailComponent,
    AnswerForLostItemsComponent,
    FoundItemCardComponent,
    FoundItemDetailComponent,
    FoundItemListComponent,
    FoundItemsPageComponent,
    UserComponent,
    LostItemCreatorComponent,
    DragDropDirective,
    FoundItemCreatorComponent



  ],
  imports: [
    NgbModule,
    BrowserModule,
    FormsModule ,
    RoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatSelectModule,
    MatDialogModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatMenuModule,
    MatNativeDateModule,
    FlexLayoutModule,
    HttpClientModule,
    CommonModule,
    NgMultiSelectDropDownModule,
    RouterModule,
    NgMultiSelectDropDownModule.forRoot(),
    NgxPageScrollCoreModule,
    NgxPageScrollModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }


