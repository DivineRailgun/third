package projeszk.lostbutfound.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import projeszk.lostbutfound.domain.Label;
import projeszk.lostbutfound.service.PlaceService;

import java.util.List;


/**
 * Controller
 */
@CrossOrigin
@RestController
@RequestMapping("places")
public class PlaceController {

    @Autowired
    private PlaceService service;

    @GetMapping(value = "", produces = "application/json")
    public List<Label> getAllLabels() {
        return service.getAllLabels();
    }
}