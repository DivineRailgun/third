package projeszk.lostbutfound.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import projeszk.lostbutfound.domain.LostItem;
import projeszk.lostbutfound.service.LostItemService;

import java.util.List;


/**
 * Controller
 */
@CrossOrigin
@RestController
@RequestMapping("lost_items")
public class LostItemController {

  @Autowired
  private LostItemService service;

  @GetMapping(value = "/{id}", produces = "application/json")
  public LostItem getLostItemById(@PathVariable Long id) {
    return service.getLostItemById(id);
  };

  @GetMapping(value = "", produces = "application/json")
  public List<LostItem> getAllLostItems() {
    return service.getAllLostItems();
  }

  @PostMapping(value = "", produces = "application/json")
  public void createLostItem(@RequestBody LostItem rb) {
    service.createLostItem(rb);
  }

  @DeleteMapping(value = "/{id}", produces = "application/json")
  public void deleteLostItemById(@PathVariable Long id) {
    service.deleteLostItemById(id);
  }

}