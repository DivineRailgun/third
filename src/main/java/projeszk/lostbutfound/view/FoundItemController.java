package projeszk.lostbutfound.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import projeszk.lostbutfound.domain.FoundItem;
import projeszk.lostbutfound.service.FoundItemService;

import java.util.List;


/**
 * Controller
 */
@CrossOrigin
@RestController
@RequestMapping("found_items")
public class FoundItemController {

  @Autowired
  private FoundItemService service;

  @GetMapping(value = "/{id}", produces = "application/json")
  public FoundItem getFoundItemById(@PathVariable Long id) {
    return service.getFoundItemById(id);
  }

  @GetMapping(value = "", produces = "application/json")
  public List<FoundItem> getAllFoundItems() {
    return service.getAllFoundItems();
  }

  @PostMapping(value = "", produces = "application/json")
  public void createFoundItem(@RequestBody FoundItem rb) {
    service.createFoundItem(rb);
  }

  @DeleteMapping(value = "/{id}", produces = "application/json")
  public void deleteFoundItemById(@PathVariable Long id) {
    service.deleteFoundItemById(id);
  }



}