package projeszk.lostbutfound.domain;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Data
public class LostItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    private String picture;

    @OneToOne(mappedBy = "lostItem")
    private Place lostItHere;

    private String lostItHereDescription;

    private String itemDescription;

    @OneToOne(mappedBy = "lostItem")
    private User searcher;

    private LocalDate dateOfLost;

    @ElementCollection
    private List<String> answers;

    @OneToMany(mappedBy = "lostItem")
    List<Label> labels;
}
