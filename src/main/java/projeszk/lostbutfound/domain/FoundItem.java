package projeszk.lostbutfound.domain;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
public class FoundItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    private String picture;

    @OneToOne(mappedBy = "foundItem")
    private Place foundItHere;

    private String foundItHereDescription;

    @OneToOne(mappedBy = "foundItem")
    private Place receivingPlace;

    private String itemDescription;

    @OneToOne(mappedBy = "foundItem")
    private User founder;

    private LocalDate dateOfFound;
}
