package projeszk.lostbutfound.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Place {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @OneToOne
    @JoinColumn(name = "found_it_here")
    FoundItem foundItem;

    @OneToOne
    @JoinColumn(name = "lost_it_here")
    LostItem lostItem;
}
