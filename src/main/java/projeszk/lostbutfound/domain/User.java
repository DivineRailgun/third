package projeszk.lostbutfound.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;
    String username;

    String password;

    String fullName;

    @OneToOne
    @JoinColumn(name = "founder")
    FoundItem foundItem;

    @OneToOne
    @JoinColumn(name = "searcher")
    LostItem lostItem;

    //List<LostItem> lostItems;
    //List<foundItem> foundItems;
}
