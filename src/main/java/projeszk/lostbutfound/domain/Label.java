package projeszk.lostbutfound.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Label {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String id;

    private String name;

    @ManyToOne
    @JoinColumn(name = "labels")
    private LostItem lostItem;
}
