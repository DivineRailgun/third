package projeszk.lostbutfound.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import projeszk.lostbutfound.dao.LostItemRepository;
import projeszk.lostbutfound.domain.LostItem;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service
 */
@Service
public class LostItemService {

  @Autowired
  private LostItemRepository repository;

  public LostItem getLostItemById(Long id) {
      return repository.findById(id).orElse(null);
  }

  public List<LostItem> getAllLostItems() {
    return StreamSupport.stream(
        repository.findAll().spliterator(), false)
        .collect(Collectors.toList());
  }

  public void createLostItem(LostItem lostItem) {
        repository.save(lostItem);
    }

    public void deleteLostItemById(Long id) {
        repository.deleteById(id);
    }
}
