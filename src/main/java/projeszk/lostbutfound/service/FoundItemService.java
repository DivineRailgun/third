package projeszk.lostbutfound.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import projeszk.lostbutfound.dao.FoundItemRepository;
import projeszk.lostbutfound.domain.FoundItem;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service
 */
@Service
public class FoundItemService {

  @Autowired
  private FoundItemRepository repository;

  public FoundItem getFoundItemById(Long id) {
    return repository.findById(id).orElse(null);
  }

  public List<FoundItem> getAllFoundItems() {
    return StreamSupport.stream(
        repository.findAll().spliterator(), false)
            .collect(Collectors.toList());
  }

  public void createFoundItem(FoundItem foundItem) {
    repository.save(foundItem);
  }

  public void deleteFoundItemById(Long id) {
    repository.deleteById(id);
  }
}
