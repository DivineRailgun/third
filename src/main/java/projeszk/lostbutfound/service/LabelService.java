package projeszk.lostbutfound.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import projeszk.lostbutfound.dao.LabelRepository;
import projeszk.lostbutfound.domain.Label;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class LabelService {

  @Autowired
  private LabelRepository repository;

  public List<Label> getAllLabels() {
    return StreamSupport.stream(
        repository.findAll().spliterator(), false)
        .collect(Collectors.toList());
  }
}
