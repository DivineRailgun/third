package projeszk.lostbutfound.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import projeszk.lostbutfound.dao.PlaceRepository;
import projeszk.lostbutfound.domain.Label;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class PlaceService {

    @Autowired
    private PlaceRepository repository;

    public List<Label> getAllLabels() {
        return StreamSupport.stream(
                repository.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }
}
