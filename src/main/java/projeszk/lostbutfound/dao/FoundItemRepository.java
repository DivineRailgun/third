package projeszk.lostbutfound.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import projeszk.lostbutfound.domain.FoundItem;

import java.util.Optional;

/**
 * Repository
 */
@Repository
public interface FoundItemRepository extends CrudRepository<FoundItem, Long> {
  Optional<FoundItem> findById(Long id);

  Iterable<FoundItem> findAll();

  FoundItem save(FoundItem foundItem);

  void deleteById(Long id);
}
