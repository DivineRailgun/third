package projeszk.lostbutfound.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import projeszk.lostbutfound.domain.Label;

@Repository
public interface LabelRepository extends CrudRepository<Label, Long> {
  Iterable<Label> findAll();
}
