package projeszk.lostbutfound.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import projeszk.lostbutfound.domain.LostItem;

import java.util.Optional;

/**
 * Repository
 */
@Repository
public interface LostItemRepository extends CrudRepository<LostItem, Long> {
    Optional<LostItem> findById(Long id);

    Iterable<LostItem> findAll();

    LostItem save(LostItem lostItem);

    void deleteById(Long id);
}
